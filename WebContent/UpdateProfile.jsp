<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dashboard</title>
<!-- Bootstrap core CSS-->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="vendor/datatables/dataTables.bootstrap4.css"
	rel="stylesheet">
<!-- Custom styles for this template-->
<link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
	<!-- Navigation-->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top"
		id="mainNav"> <a class="navbar-brand" href="Dashboard.jsp">One
		Point Solution</a>
	<button class="navbar-toggler navbar-toggler-right" type="button"
		data-toggle="collapse" data-target="#navbarResponsive"
		aria-controls="navbarResponsive" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarResponsive">
		<ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
			<li class="nav-item" data-toggle="tooltip" data-placement="right"
				title="Dashboard"><a class="nav-link" href="Dashboard.jsp">
					<i class="fa fa-fw fa-dashboard"></i> <span class="nav-link-text">Update Profile</span>
			</a></li>
			<li class="nav-item" data-toggle="tooltip" data-placement="right"
				title="Lodge Complaint"><a class="nav-link" href="lodge.jsp">
					<i class="fa fa-fw fa-area-chart"></i> <span class="nav-link-text">Lodge
						Complaint</span>
			</a></li>
			<li class="nav-item" data-toggle="tooltip" data-placement="right"
				title="View Complaints"><a class="nav-link" href="viewcomplains.jsp">
					<i class="fa fa-fw fa-table"></i> <span class="nav-link-text">View
						Complaints</span>
			</a></li>
			<li class="nav-item" data-toggle="tooltip" data-placement="right"
				title="View Status"><a class="nav-link" href="viewstatus.jsp">
					<i class="fa fa-fw fa-table"></i> <span class="nav-link-text">View
						Status</span>
			</a></li>
			<li class="nav-item" data-toggle="tooltip" data-placement="right"
				title="Give Feedback"><a class="nav-link" href="feedback.jsp"> <i
					class="fa fa-fw fa-table"></i> <span class="nav-link-text">Give
						Feedback</span>
			</a>
			<li class="nav-item" data-toggle="tooltip" data-placement="right"
				title="Update Profile"><a class="nav-link" href="#"> <i
					class="fa fa-fw fa-link"></i> <span class="nav-link-text">Update
						Profile</span>
			</a></li>
			<li class="nav-item" data-toggle="tooltip" data-placement="right"
				title="Change Password"><a class="nav-link" href="changepass.jsp"> <i
					class="fa fa-fw fa-link"></i> <span class="nav-link-text">Change
						Password</span>
			</a></li>
		</ul>
		<ul class="navbar-nav sidenav-toggler">
			<li class="nav-item"><a class="nav-link text-center"
				id="sidenavToggler"> <i class="fa fa-fw fa-angle-left"></i>
			</a></li>
		</ul>
		<ul class="navbar-nav ml-auto">

			<li class="nav-item"><a class="nav-link" data-toggle="modal"
				data-target="#exampleModal"> <i class="fa fa-fw fa-sign-out"></i>Logout
			</a></li>
		</ul>
	</div>
	</nav>
	<div class="content-wrapper">
		<div class="container-fluid">
			<!-- Add from Here -->
			<form action="register" method="post">
				<div class="form-group">
					<div class="form-row">
						<div class="col-md-6">
							<label for="exampleInputName">First name</label> <input
								class="form-control" id="exampleInputName" type="text"
								aria-describedby="nameHelp" placeholder="Enter first name"
								name="fn">
						</div>
						<div class="col-md-6">
							<label for="exampleInputLastName">Last name</label> <input
								class="form-control" id="exampleInputLastName" type="text"
								aria-describedby="nameHelp" placeholder="Enter last name"
								name="ln">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="exampleInputEmail1">Email address</label> <input
						class="form-control" id="exampleInputEmail1" type="email"
						aria-describedby="emailHelp" placeholder="Enter email"
						name="email">
				</div>
				<div class="form-group">
					<label for="exampleInputEmail1">Contact</label> <input
						class="form-control" id="exampleInputEmail1" type="text"
						aria-describedby="emailHelp" placeholder="Enter email"
						name="phone">
				</div>
				<div class="form-group">
					<div class="form-row">
						<div class="col-md-6">
							<label for="exampleInputPassword1">Password</label> <input
								class="form-control" id="exampleInputPassword1" type="password"
								placeholder="Password" name="pass1">
						</div>
						<div class="col-md-6">
							<label for="exampleConfirmPassword">Confirm password</label> <input
								class="form-control" id="exampleConfirmPassword" type="password"
								placeholder="Confirm password" name="pass2">
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="form-row">
						<div class="col-md-6">
							<label for="exampleInputPassword1">Select Your Department</label>
							<select class="form-control" name="dept">
								<option>Select an Option</option>
								<option value="B.Tech Civil Engg">B.Tech Civil Engg</option>
								<option value="B.Tech Mechanical Engg">B.Tech
									Mechanical Engg</option>
								<option value="B.Tech Computer Science Engg">B.Tech
									Computer Science Engg</option>
								<option value="B.Tech Electrical Engg">B.Tech
									Electrical Engg</option>
								<option value="B.Tech Electronics Engg">B.Tech
									Electronics Engg</option>
								<option value="B.Tech Mechatronics Engg">B.Tech
									Mechatronics Engg</option>
								<option value="B.Tech BioTech">B.Tech BioTech</option>
								<option value="MBBS">MBBS</option>
								<option value="MD">MD</option>
								<option value="MLT">MLT</option>
								<option value="BDS">BDS</option>
								<option value="MDS">MDS</option>
								<option value="Pharmacy">Pharmacy</option>
								<option value="Nursing">Nursing</option>
								<option value="BBA">BBA</option>
								<option value="MBA">MBA</option>
								<option value="B.COM">B.COM</option>
								<option value="M.COM">M.COM</option>
								<option value="Law">Law</option>
								<option value="Hotel Management">Hotel Management</option>
								<option value="Food Technology">Food Technology</option>
								<!-- Resume From here -->
							</select>
						</div>

					</div>
				</div>
				<input type="submit" class="btn btn-primary btn-block col-md-3"
					value="Update Profile">
			</form>
			<!-- Add upto Here -->
			<div></div>

			<!-- /.container-fluid-->
			<!-- /.content-wrapper-->
			<footer class="sticky-footer">
			<div class="container">
				<div class="text-center">
					<small>Copyright � One Point Solutions 2018</small>
				</div>
			</div>
			</footer>
			<!-- Scroll to Top Button-->
			<a class="scroll-to-top rounded" href="#page-top"> <i
				class="fa fa-angle-up"></i>
			</a>
			<!-- Logout Modal-->
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
				aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Ready to
								Leave?</h5>
							<button class="close" type="button" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">�</span>
							</button>
						</div>
						<div class="modal-body">Select "Logout" below if you are
							ready to end your current session.</div>
						<div class="modal-footer">
							<button class="btn btn-secondary" type="button"
								data-dismiss="modal">Cancel</button>
							<form method="post" action="Dismiss">
            <input class="btn btn-white" type="submit" value="Log Out">
            </form>
						</div>
					</div>
				</div>
			</div>
			<!-- Bootstrap core JavaScript-->
			<script src="vendor/jquery/jquery.min.js"></script>
			<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
			<!-- Core plugin JavaScript-->
			<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
			<!-- Page level plugin JavaScript-->
			<script src="vendor/chart.js/Chart.min.js"></script>
			<script src="vendor/datatables/jquery.dataTables.js"></script>
			<script src="vendor/datatables/dataTables.bootstrap4.js"></script>
			<!-- Custom scripts for all pages-->
			<script src="js/sb-admin.min.js"></script>
			<!-- Custom scripts for this page-->
			<script src="js/sb-admin-datatables.min.js"></script>
			<script src="js/sb-admin-charts.min.js"></script>
		</div>
</body>

</html>
