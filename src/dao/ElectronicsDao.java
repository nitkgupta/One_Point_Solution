package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import beans.Bean;
import utilities.DatabaseConn;

public class ElectronicsDao {
		Connection con = DatabaseConn.getConnection();
		public boolean create(Bean bean)
		{
			boolean flag = false;
			
			try {
				PreparedStatement ps = con.prepareStatement("insert into Electronics values(?,?,?,?,?,?,?,?)");
				ElectronicsDao dd=new ElectronicsDao();
				String dId="Electronics@"+dd.sequence();
				dd.Linking(bean.getId(), dId);
				ps.setInt(1, bean.getRoll_no());
				ps.setString(2, bean.getName());
				ps.setString(3, bean.getEmail());
				ps.setString(4, bean.getComplaint());
				ps.setDate(5, bean.getDateofComplaint());
				ps.setString(6, "Pending");
				ps.setString(7, dId);
				ps.setString(8, "");
				int i = ps.executeUpdate();
				if(i==1)
				{
					flag = true;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return flag;
		}
		
		public boolean delete(int compid)
		{
			boolean flag = false;
			try {
				PreparedStatement ps = con.prepareStatement("delete from electronics where complaintid = ?");
				ps.setInt(1, compid);
				int i = ps.executeUpdate();
				if(i==1)
				{
					flag = true;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return flag;
		}
		
		public Bean find(int rollno)
		{
			Bean b = null;
			try {
				PreparedStatement ps = con.prepareStatement("select * from electronics where rollno = ?");
				ps.setInt(1, rollno);
				ResultSet rs = ps.executeQuery();
				if(rs.next())
				{
					b = new Bean();
					b.setRollno(rollno);
					b.setName(rs.getString("name"));
					b.setEmail(rs.getString("email"));
					b.setDateofComplaint(rs.getDate("dataOfComplaint"));
					b.setComplaint(rs.getString("complaint"));
					b.setComplaintStatus(rs.getString("status"));
					b.setReply(rs.getString("reply"));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return b;
			
		}
		public boolean updateStatus(int compid , String newStatus)
		{
			boolean flag = false;
			try {
				PreparedStatement ps = con.prepareStatement("update electronics set status =? where complaintid = ? ");
				ps.setString(1, newStatus);
				ps.setInt(2, compid);
				int i = ps.executeUpdate();
				if(i==1)
				{
					flag = true;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return flag;
			
		}
		public boolean updateReply(int compid , String reply)
		{
			boolean flag = false;
			try {
				PreparedStatement ps = con.prepareStatement("update electronics set reply =? where complaintid = ? ");
				ps.setString(1, reply);
				ps.setInt(2, compid);
				int i = ps.executeUpdate();
				if(i==1)
				{
					flag = true;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return flag;
			
		}
		
		public ArrayList<Bean> findAll()
		{
			ArrayList<Bean> al = new ArrayList<Bean>();
			PreparedStatement ps;
			try {
				ps = con.prepareStatement("select * from electronics");
				ResultSet rs = ps.executeQuery();
				while(rs.next())
				{
					Bean b = new Bean();
					b.setRollno(rs.getInt(1));
					b.setName(rs.getString(2));
					b.setEmail(rs.getString(3));
					b.setComplaint(rs.getString(4));
					b.setDateofComplaint(rs.getDate(5));
					b.setComplaintStatus(rs.getString(6));
					b.setComplaintId(rs.getString(7));
					b.setReply(rs.getString(8));
					al.add(b);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			return al;
		}
		public int i;

		public int sequence() {

			try {
				Statement st = con.createStatement();
				ResultSet rs = st.executeQuery("select onesol.nextval from dual");
				while (rs.next())
					i = rs.getInt(1);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return i;

		}
		public String Linking(String id,String dId) throws SQLException{
			PreparedStatement ps=con.prepareStatement("insert into linking values(?,?)");
			ps.setString(1, id);
			ps.setString(2, dId);
			int i=ps.executeUpdate();
			return dId;
			
		}
}
