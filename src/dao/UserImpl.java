package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import utilities.DatabaseConn;
import beans.UserBean;

public class UserImpl implements User {
	// Gets DB Connection object from con.ata.util.DBUtil
	Connection con = DatabaseConn.getConnection();

	@Override
	public String createUser(UserBean bean) {
		try {
			
			int max = 0;
			UserImpl imp=new UserImpl();
			max = imp.sequence();
			String usr=(bean.getName()).substring(0, 3);
			String id = usr + max;

			PreparedStatement ps = con.prepareStatement("INSERT INTO users VALUES (?,?,?,?,?,?,?,?)");
			ps.setString(1, id);
			ps.setString(2, bean.getName());
			ps.setString(3, bean.getPassword());
			ps.setString(4, bean.getEmail());
			ps.setLong(5, bean.getPhone());
			ps.setString(6, bean.getDepartment());
			ps.setString(7, bean.getUsrtype());
			ps.setInt(8, max);

			int a = ps.executeUpdate();
			if (a > 0) {
				return id;
			} else {
				return "FAIL";
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "FAIL";
	}

	@Override
	public int deleteUser(ArrayList<String> li) {
		int r1 = 0;
		for (String userId : li) {

			try {
				PreparedStatement ps = con.prepareStatement("DELETE FROM users where userid=?");
				ps.setString(1, userId);
				r1 = ps.executeUpdate();
				if(r1>0)
					return 1;

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}

	@Override
	public boolean updateUser(UserBean bean) {
		try {
			PreparedStatement ps = con.prepareStatement(
					"UPDATE User SET name=?, password=?,email=?,phone=?,department=? where userId=?");
			ps.setString(1, bean.getName());
			ps.setString(2, bean.getPassword());
			ps.setString(3, bean.getEmail());
			ps.setLong(4, bean.getPhone());
			ps.setString(5,bean.getDepartment());

			int r1 = ps.executeUpdate();
			if (r1 > 0)
				return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public UserBean findByID(String userId) {
		try {
			PreparedStatement ps = con.prepareStatement("SELECT * FROM users WHERE userId=?");
			ps.setString(1, userId);
			ResultSet rs = ps.executeQuery();
			UserBean bean = new UserBean();
				while(rs.next()) {
				bean.setUid(rs.getString(1));
				bean.setName(rs.getString(2));
				bean.setPassword(rs.getString(3));
				bean.setEmail(rs.getString(4));
				bean.setPhone(rs.getLong(5));
				bean.setDepartment(rs.getString(6));
				bean.setUsrtype(rs.getString(7));
				}
			return bean;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public ArrayList<UserBean> findAll() {
		ArrayList<UserBean> li = new ArrayList<UserBean>();
		try {
			PreparedStatement ps = con.prepareStatement("SELECT * FROM User");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				UserBean bean = new UserBean();bean.setUid(rs.getString(1));
				bean.setName(rs.getString(2));
				bean.setPassword(rs.getString(3));
				bean.setEmail(rs.getString(4));
				bean.setPhone(rs.getLong(5));
				bean.setDepartment(rs.getString(6));
				li.add(bean);
			}
			return li;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public int sequence() {
		int i = 0;

		try {
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("select memb.nextval from dual");
			while (rs.next())
				i = rs.getInt(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return i;

	}
}
