package dispatch;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utilities.DatabaseConn;

/**
 * Servlet implementation class DataServe
 */
public class DataServe extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String str = request.getParameter("dept");
		HttpSession ses=request.getSession();
		ses.setAttribute("ment", str);
		String str1 = "SELECT " + str + ".COMPLAINTID, " + str + ".NAME, " + str + ".ROLLNO, " + str + ".COMPLAINT, "
				+ str + ".STATUS, " + str + ".REPLY FROM " + str + " LEFT JOIN linking ON " + str
				+ ".COMPLAINTID = linking.COMPLAINTID";
		try {
			PreparedStatement st = DatabaseConn.getConnection().prepareStatement(str1);
			ResultSet rs = st.executeQuery();
			PrintWriter pw = response.getWriter();
			String rep=null;
			while (rs.next()) {
				pw.print("<tr>");
				pw.print("<td>" + rs.getString("COMPLAINTID") + "</td>" + "<td>" + rs.getString("NAME") + "</td>"
						+ "<td>" + rs.getInt("ROLLNO") + "</td>" + "<td>" + rs.getString("COMPLAINT") + "<td>"
						+ rs.getString("STATUS") + "</td>" + "<td>" + rs.getString("REPLY") + "</td>");
				pw.print("</tr>");
			}
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
