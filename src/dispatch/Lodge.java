package dispatch;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Bean;
import dao.*;
import utilities.Mailer;
/**
 * Servlet implementation class Lodge
 */
public class Lodge extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String fn=request.getParameter("fn");
		String ln=request.getParameter("ln");
		String name=fn+" "+ln;
		int roll=Integer.parseInt(request.getParameter("roll"));
		String email=request.getParameter("email");
		String contact=request.getParameter("phone");
		String complaint=request.getParameter("comp");
		String department=request.getParameter("dept");
		HttpSession hs=request.getSession();
		
		boolean b = false;
		Bean bean=new Bean();
		bean.setId(hs.getAttribute("uid").toString());
		bean.setName(name);
		bean.setRollno(roll);
		bean.setComplaint(complaint);
		Date dt=new Date(10000);
		bean.setDateofComplaint(dt);
		bean.setEmail(email);
		if(department.equals("civil")){
			CivilDao dao=new CivilDao();
			b=dao.create(bean);
		}
		else if(department.equals("bba")){
			BBADao dao=new BBADao();
			b=dao.create(bean);
		}
		else if(department.equals("bcom")){
			BCOMDao dao=new BCOMDao();
			b=dao.create(bean);
		}
		else if(department.equals("biotech")){
			BiotechDao dao=new BiotechDao();
			b=dao.create(bean);
		}
		else if(department.equals("computer")){
			ComputerDao dao=new ComputerDao();
			b=dao.create(bean);
		}
		else if(department.equals("electrical")){
			ElectricalDao dao=new ElectricalDao();
			b=dao.create(bean);
		}
		else if(department.equals("electronics")){
			ElectronicsDao dao=new ElectronicsDao();
			b=dao.create(bean);
		}
		else if(department.equals("hotel")){
			HotelDao dao=new HotelDao();
			b=dao.create(bean);
		}
		else if(department.equals("law")){
			LawDao dao=new LawDao();
			b=dao.create(bean);
		}
		else if(department.equals("mba")){
			MBADao dao=new MBADao();
			b=dao.create(bean);
		}
		else if(department.equals("mbbs")){
			MbbsDao dao=new MbbsDao();
			b=dao.create(bean);
		}
		else if(department.equals("mcom")){
			MCOMDao dao=new MCOMDao();
			b=dao.create(bean);
		}
		else if(department.equals("md")){
			MdDao dao=new MdDao();
			b=dao.create(bean);
		}
		else if(department.equals("mechanical")){
			MechanicalDao dao=new MechanicalDao();
			b=dao.create(bean);
		}
		else if(department.equals("mechatronics")){
			MechatronicsDao dao=new MechatronicsDao();
			b=dao.create(bean);
		}
		else if(department.equals("mlt")){
			MltDao dao=new MltDao();
			b=dao.create(bean);
		}
		else if(department.equals("nursing")){
			NursingDao dao=new NursingDao();
			b=dao.create(bean);
		}
		else if(department.equals("pharmacy")){
			PharmacyDao dao=new PharmacyDao();
			b=dao.create(bean);
		}
		//		System.out.println(b);
		if(b){
			RequestDispatcher rd=request.getRequestDispatcher("successlodge.jsp");
			Mailer mail=new Mailer();
			mail.send("info.1ptsol@gmail.com", "nitsnemay", email, "Complaint Ticket Raised", "Your Complaint ticket has been raised successfully. It will be  monitored in short time.Check your dashboard for more details.");

			rd.forward(request, response);
		}
		}

}
