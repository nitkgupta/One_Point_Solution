package dispatch;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utilities.DatabaseConn;

/**
 * Servlet implementation class Repcom
 */
public class Repcom extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String ids=request.getParameter("gid");
		String rep=request.getParameter("rep");
		HttpSession hss=request.getSession();
		String ment1=hss.getAttribute("ment").toString();
		System.out.println(ids+"    "+rep+"    "+ment1);
		try {
			Statement st = DatabaseConn.getConnection().createStatement();
			String ab="update "+ment1+" set REPLY='"+rep+"',STATUS='Completed' where COMPLAINTID='"+ids+"'";
			int i=st.executeUpdate(ab);
			DatabaseConn.getConnection().commit();
			System.out.println(i);
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
