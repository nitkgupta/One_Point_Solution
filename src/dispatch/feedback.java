package dispatch;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utilities.DatabaseConn;
import utilities.Mailer;

/**
 * Servlet implementation class feedback
 */
public class feedback extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name=request.getParameter("name");
		String email=request.getParameter("email");
		String message=request.getParameter("message");
		DatabaseConn db=new DatabaseConn();
		Connection con=db.getConnection();
		try {
			PreparedStatement ps=con.prepareStatement("insert into feedback values(?,?,?)");
			ps.setString(1, name);
			ps.setString(2, email);
			ps.setString(3, message);
			int i=ps.executeUpdate();
			Mailer mail = new Mailer();
			
			if(i>0) {

				mail.send("info.1ptsol@gmail.com", "nitsnemay", email, "Feedback", "Thanks for your precious feedback!");
				response.sendRedirect("Dashboard.jsp");
			}
			else
				System.out.println("Some error occured while submitting");
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
	}

}
