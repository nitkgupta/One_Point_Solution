package login;

import beans.UserBean;

public interface Authentication {
	boolean authenticate(UserBean bean);
	int authorize(String userID);
	boolean changeLoginStatus(UserBean bean, int loginStatus);
}
