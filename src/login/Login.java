
package login;
import java.io.IOException;
import java.security.Security;
import java.sql.Connection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBean;
import dao.UserImpl;
import utilities.DESedeEncryptDataString;
import utilities.DESedeEncryptDataString.EncryptionException;
import utilities.DatabaseConn;
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String uid = request.getParameter("uid");
		String pwd = request.getParameter("pwd");
		Connection con=DatabaseConn.getConnection();
		UserBean bean=new UserBean();
		UserImpl ui=new UserImpl();
		bean=ui.findByID(uid);
        Security.addProvider(new com.sun.crypto.provider.SunJCE());
        final byte[] keyBytes ="0123456789ABCDEF01234567".getBytes("ASCII");
        final byte[] IVBytes = "ABCDEFGH".getBytes("ASCII");
        DESedeEncryptDataString dataStringEncryptAgent;
		try {
			dataStringEncryptAgent = new DESedeEncryptDataString(keyBytes, IVBytes, "UTF-8");
	        String encoded = dataStringEncryptAgent.encrypt(pwd);
	        String pass=bean.getPassword();
	        String type=bean.getUsrtype();
	        if(pass.equals(encoded) && type.equals("superadmin"))
	        {
	        	RequestDispatcher rd = request.getRequestDispatcher("Dashboard.jsp");
	        	HttpSession hs=request.getSession();
	        	hs.setAttribute("uid", uid);
	        	rd.forward(request,response);
	        }
	        else if(pass.equals(encoded) && type.equals("a"))
	        {
	        	RequestDispatcher rd = request.getRequestDispatcher("sDashboard.jsp");
	        	HttpSession hs=request.getSession();
	        	hs.setAttribute("uid", uid);
	        	rd.forward(request,response);
	        }
	        else if(pass.equals(encoded) && type.equals("b"))
	        {
	        	RequestDispatcher rd = request.getRequestDispatcher("Dashboard.jsp");
	        	HttpSession hs=request.getSession();
	        	hs.setAttribute("uid", uid);
	        	rd.forward(request,response);
	        }
	        else
	        {
	        	response.sendRedirect("LogError.html");
	        }

		
		} catch (EncryptionException e) {
			e.printStackTrace();
		}
	}

}
