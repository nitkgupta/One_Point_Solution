package login;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.Security;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserBean;
import dao.UserImpl;
import utilities.DESedeEncryptDataString;
import utilities.DESedeEncryptDataString.EncryptionException;
import utilities.DatabaseConn;
import utilities.Mailer;

public class register extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String name1 = request.getParameter("fn");
		String name2 = request.getParameter("ln");
		String name = name1 + " " + name2;
		long phone = Long.parseLong(request.getParameter("phone"));
		String email = request.getParameter("email");
		String dept = request.getParameter("dept");
		String usr = request.getParameter("user");
		String pass = request.getParameter("pass1");
//		
		String encoded = null;
		Security.addProvider(new com.sun.crypto.provider.SunJCE());
        final byte[] keyBytes ="0123456789ABCDEF01234567".getBytes("ASCII");
        final byte[] IVBytes = "ABCDEFGH".getBytes("ASCII");
        DESedeEncryptDataString dataStringEncryptAgent;
    	try {
			dataStringEncryptAgent = new DESedeEncryptDataString(keyBytes, IVBytes, "UTF-8");
			encoded=dataStringEncryptAgent.encrypt(pass);
		} catch (EncryptionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
//		
		PrintWriter ps = response.getWriter();
		System.out.println(name + phone + email + dept + usr + pass);
		UserBean bean = new UserBean();
		bean.setEmail(email);
		bean.setName(name);
		bean.setDepartment(dept);
		bean.setPassword(encoded);
		bean.setUsrtype(usr);
		bean.setPhone(phone);
		UserImpl imp = new UserImpl();
		String check = imp.createUser(bean);
		if (check.equals("FAIL")) {
			ps.println("User Registration Failed");
		} else {
			try {
				// from,password,to,subject,message
				Mailer mail = new Mailer();
				mail.send("info.1ptsol@gmail.com", "nitsnemay", email, "Successfull Registration",
						"Your account has been successfully created. Your id no is " + check
								+ ". Please Remember this id");

			} catch (RuntimeException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
			ps.println("Hooray!!User Created Successfully");
			ps.println("Please return to front page to login");
		}

	}

}
